# @cougargrades/web

![Build and Deploy - Testing](https://github.com/cougargrades/web/workflows/Build%20and%20Deploy%20-%20Testing/badge.svg) ![Build and Deploy - Production](https://github.com/cougargrades/web/workflows/Build%20and%20Deploy%20-%20Production/badge.svg) ![ESLint](https://github.com/cougargrades/web/workflows/ESLint/badge.svg) ![Mirroring](https://github.com/cougargrades/web/workflows/Mirroring/badge.svg)

React app that powers cougargrades.io

## Project Board

See: https://github.com/orgs/cougargrades/projects/2

## Project Status

Currently, the entire CougarGrades.io stack is near finished with a major site overhaul. This means that the code you find here may not look like the live website.

| Project                                                                              	| Version 	| Status                                                              	| URL                                       	|
|--------------------------------------------------------------------------------------	|---------	|---------------------------------------------------------------------	|-------------------------------------------	|
| cougargrades.io front-end, live version (as of Nov 2020)                             	| 0.4.4   	| Live 🚀                                                              	| https://cougargrades.io                   	|
| [**cougargrades.io front-end, major redesign**](https://github.com/cougargrades/web) 	| 1.0.0   	| [In Progress 👨‍💻](https://github.com/orgs/cougargrades/projects/2)   	| https://cougargrades-testing.web.app      	|
| cougargrades.io API, original version                                                	| 1.1.0   	| Offline                                                             	| https://cougargrades.io/api/              	|
| [**cougargrades.io API, concurrent rework**](https://github.com/cougargrades/api)    	| 2.0.0   	| [Near Complete 👨‍💻](https://github.com/orgs/cougargrades/projects/1) 	| https://cougargrades-testing.web.app/api/ 	|

### Continuous Deployment

Active commits are automatically deployed to https://cougargrades-testing.web.app for preview.

---

[![Powered by Vercel](public/powered-by-vercel.svg)](https://vercel.com/?utm_source=cougargrades&utm_campaign=oss)